## Versions

### 0.78.2 [Win](https://gitlab.com/viastore-software-gmbh/viadat-vision/frontend-desktop/-/jobs/artifacts/0.78.2/download?job=build+for+windows) [Win-docker](https://gitlab.com/viastore-software-gmbh/viadat-vision/frontend-desktop/-/jobs/artifacts/0.78.2/download?job=build-docker-StandaloneWindows64)

### 0.78.1 [Win](https://gitlab.com/viastore-software-gmbh/viadat-vision/frontend-desktop/-/jobs/artifacts/0.78.1/download?job=build+for+windows) [Win-docker](https://gitlab.com/viastore-software-gmbh/viadat-vision/frontend-desktop/-/jobs/artifacts/0.78.1/download?job=build-docker-StandaloneWindows64)

### 0.78.0 [Win](https://gitlab.com/viastore-software-gmbh/viadat-vision/frontend-desktop/-/jobs/artifacts/0.78.0/download?job=build+for+windows) [Win-docker](https://gitlab.com/viastore-software-gmbh/viadat-vision/frontend-desktop/-/jobs/artifacts/0.78.0/download?job=build-docker-StandaloneWindows64)

